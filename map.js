export class map{
    // 0 case libre
    // 1 ennemi
    // 2 sortie
    tab = [
        [0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 1, 0, 1, 0, 0, 0],
        [1, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 2, 0, 0, 0, 0, 0, 0]
        
    ]

    constructor(){


    }

    verification(x, y){

        // Détection de la case
        // x:0 y:1
        let cell = this.tab[y][x]

        if(cell == 0){
            console.log("Case libre")
        }
        else if(cell == 1){
            console.log("Ennemi")
        }
        else if(cell == 2){
            console.log("Bravo vous êtes sorti !!")
        }
    }
}