export class Hero {
    
    name = 'Théo'
    x = 0
    y = 0
    oldX = 0
    oldY = 0
    att = 20
    def = 10
    hp = 20
    hpmax = 50
    
    
    constructor(){


    }
    move(direction){

        if(direction == "bas") {
            this.y++
        }
        else if(direction == "haut") {
            this.y--
        }
        else if(direction == "gauche") {
            this.x--
        }
        else if(direction == "droite") {
            this.x++
        }
    }


}
