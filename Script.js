
import {Hero} from "./Hero.js";
import {map} from "./map.js";

// Instanciations
const perso = new Hero();
const carte = new map();

// Debug
/*console.log("Positions début héro:");
console.log("x:"+perso.x);
console.log("y:"+perso.y);
*/

// Méthode move()
/*perso.move("bas");
perso.move("droite");
carte.verification(perso.x, perso.y);
*/

// Debug
/*console.log("Positions fin héro:");
console.log("x:"+perso.x);
console.log("y:"+perso.y);
*/

//Déplacement
const log = document.getElementById('log');
document.addEventListener('keydown', logKey);
console.table(carte.tab)

function logKey(e) {
 //console.log(e)
  perso.oldX = perso.x 
  perso.oldY = perso.y
  if (e.code == "ArrowDown"){
    if (perso.y<7){
      perso.move("bas")
    }
  }else if (e.code == "ArrowLeft"){
    if (perso.x>=1){
      perso.move("gauche")
    }
  }else if (e.code == "ArrowRight"){
    if (perso.x<7){
      perso.move("droite")
    }
  }else if (e.code == "ArrowUp"){
    if (perso.y>=1){
      perso.move("haut")
    }
  }
  console.log("x:"+perso.x);
  console.log("y:"+perso.y);
  carte.verification(perso.x, perso.y);
}